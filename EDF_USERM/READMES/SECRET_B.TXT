Duke Nukem 3D .MAP Authoring Template v1.0
================================================================
Note all my maps require duke3d atomic version plutonium pak

Title                   : Secret Base
Filename                : Secret_B.MAP
Author                  : Cedric Lutes "zaxtor znort"
Email Address           : morzenomhonx@hotmail.com or zaxtor2001@yahoo.ca
Date                    : 9-27-2003. 
                          but this is the best! (Call me fan)
Misc. Author Info       : I am 23 years old, I'm from canada
                          

Description             : Duke accidently find this weird semi lab semi factory base that uses illegal 
                          chemicals and radioactive materials that can harm the rainforest so duke must
                          destroy this base but this base is secured by scientist cops and others

Requirement             : Tile015.art file or otherwise the level will go ugly
                          Cduke3d or you wont see the cool flooring FXs
                          Be careful some enemy can shoot you so WATCH OUT!!!!


Tips                    : This is level is a bit hard not super hard i can pass
                          it without dying the size is small-mediun level about 10 to 15
                          min to pass it its full of new FXs and cool FXs

================================================================

* Play Information *

Episode and Level #     : [............]
Single Player           : Yes! 
Co-op Play              : nope!
DukeMatch Level         : nope! TOO COMPLEX
Newarts                 : yes i did and modify them all just a few of them shadowing and new textures
                         
Difficulty Settings     : N / A

* Construction *

Base                    : New level from scratch
Time of build           : this level took 100 hours to make it

Editor(s) used          : Build and  Cbuild to Make it
                          in dos mode just to fix something that would take 2 sec lol so everything is fine now
Known Bugs              : serious nope Look its my first time i make level with Cbuild and i use build to speed up the
                          construction the bug is minor HOM "hall of mirror" on the visible floor due to extreme 
                          complexity and lots and heavily sectored level


my duke site            : http://www.angelfire.com/theforce/zaxtor2001dukenukem/


