Duke Nukem 3D .MAP Authoring Template v1.0
================================================================
Note all my maps require duke3d atomic version plutonium pak

Title                   : West Alien Train
Filename                : westatrn.MAP
Author                  : Cedric Lutes "zaxtor znort"
Email Address           : morzenomhonx@hotmail.com or zaxtor2001@yahoo.ca
Date                    : 11-08-2003. 
                          but this is the best! (Call me fan)
Misc. Author Info       : I am 23 years old, I'm from
                          Canada at last is done

Description             : Duke must find an alien semi-organic train that is heading to west and wipe all those
                          aliens before they trash the next city and after duke get off that train and go in a base
                          like somesort of an underground fortress this (Part 2 of this level will be the
                          underground fortress). PS its like that level has 1316 sectors because of sprites trick
                          and intense sector joining


Tips                    :  its a fairly dangerous level so just an easy tip DONT FALL ON THE RAILS OR OFF THE TRAIN
                           also the wagon that are connect together with 2 N like poles with canons be very careful
                           go down slowly the top do like that \_ before the hole so go at the tip and jump
                           or run fast --be careful--

================================================================

* Play Information *

Episode and Level #     : [............]
Single Player           : Yes! 
Co-op Play              : nope!
DukeMatch Level         : nope!
                         
Difficulty Settings     : N / A

* Construction *

Base                    : New level from scratch
Time of build           : between 40 to 41 hours geeze man i was very fast lol

Editor(s) used          : BUILD & the build with a bigger blockmap
Known Bugs              : nope i check very often but if you find any let me know and i'll fix them :-)


my duke site            : http://www.angelfire.com/theforce/zaxtor2001dukenukem/


