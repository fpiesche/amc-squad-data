======================================================================
THIS LEVEL IS NEITHER MADE BY OR SUPPORTED BY 3D REALMS.

Title                 : Wonderful Christmas Time
Filename              : WCTime.zip
Author                : James Stanfield
E-mail                : Jstanf35@hotmail.com
Web Page              : www.planetduke.com/amc and www.geocities.com/jstanf35
Misc. Author Info     : 20 Year old who's been building for Duke for almost 10 years, but hasn't 
			released any standalone Duke maps till now (it's been almost all TCs and CBPs)
			
Other Levels/Stuff    :  Imagination World
			 CBP2, 3, 4 and 5
			 JFCBP1 and 2
			 Survival
			 Duke's 10th Anniversery Pack
			 HardG
			 Nuclear Showdown
			 Other stuff that I can't recall!

Description           : I've always wanted to make a christmas map for Duke, and DeeperThought's DukePlus was 
			enough to finally kick start my inspiration and get one done. Thanks DT!
	
			Duke has been dropped into some remote snowy mountains to investigate strange alien 
			activity. He will have to treck through the wind caves, a tourist attraction, and then
			an old mining facility before locating the source of the alien activity. Good luck, and
			Good hunting!

			Also, this map was made for Classic mode - don't play it using the HRP.

Additional Credits To : Geoffrey and Sang for Beta testing! 
			Mikko, Rob and Rusty_Nails for feedback
			Valve for making excellent games and inspiring some gameplay ideas
			DeeperThought for making Duke Plus
			3DR for making Duke3D in the first place
                          
======================================================================

* Play Information *

Episode and Level #    : User map
Single Player          : Yes
DukeMatch 2-8 Player   : No
Cooperative 2-8 Player : Yes
Difficulty Settings    : Yes (I recommend Come Get Some)
Plutonium Pak Required : Yes
New Art                : No
New Music              : Yes (The song wonderful Christmas Time)
New Sound Effects      : No
New .CON Files         : No
Demos Replaced         : No

=====================================================================

* Construction *

Base                   : New level from scratch
Level Editor(s) Used   : Mapster32
Art Editor(s) Used     : None
Construction Time      : Roughly 1 Month on and off
Known Bugs/Problems    : None that I'm aware of.

=====================================================================

*Important Information*

Installation           : This level requires Duke Plus to play properly - as of the time of 
			writing this, a download location is:

			http://www.csus.edu/indiv/g/gaskilld/DukePlus.rar

			Download and unzip Duke Plus into an atomic Duke directory first, than
			unzip WCTime.zip into that directory and run the WCTime.bat file.

Important Notes        : This level contains many of DukePlus' features like Night Vision secrets
			 and toughter enemies. If things are getting tough, try using the goggles
			 and looking for dull lights - these will flash brightly when you have the
			 goggles on. Infront of these are hidden items.

======================================================================
