
Duke Nukem 3D .MAP Authoring Template v1.0 
==================================================================
Title                  : Dracula's Castle (Eduke32 version)
Filename               : Dracula.map 
Author                 : James 'Jblade35' Stanfield
Email Address          : jstanf36@gmail.com
Misc. Author Info      : Duke modder for almost 20 years, made a whole
			 bunch of stuff like AMC TC, Nuclear Showdown,
			 Imagination World, HardG, took part in most
			 CBPs, .etc .etc

			http://www.amcwebforums.com/smf/index.php
			http://www.moddb.com/games/the-amc-tc
			http://amctc.wikia.com/wiki/AMC_TC_Wiki
			http://jbladefps.blogspot.co.uk/

Description            : Dracula's Castle; created for the Duke Spook'em
			 mod and released as a demo of sorts. Made in about 3
			 days, I set a challenge to myself to not get bogged down
                         and actually finish some maps this year. Was made to be
                         relatively open ended.

			 This is the Eduke32 version of the map, which includes a 
                         couple of reskinned enemies (Zombie pigcop from Total Meltdown,
                         and a Cthulhu Octabrain by me) as well as OGG music from 
			 Superfrog for the Amiga (recorded by Kuokka77) and all the art
			 tiles renamed to use the per-map art tile feature by helixhorned.
                          

Additional Credits to  : Micky and Mikko for beta testing.

==================================================================

* Play Information *

Episode and Level      : Dracula.map
Single Player          : Yes
Co-op                  : Yes
Dukematch Level        : Yes
Difficulty Settings    : Yes
Requirements           : Duke Nukem 3D v1.4/5 and the Tiles014 art file included
                         (If playing in Eduke32, you can rename that to DRACULA_00 and it will automatically
	                  load up when playing this map)

* Construction *

Base                   : New level from scratch.
Editor(s) used         : Mapster32
Known Bugs             : None that I know of.

 
Copyright Stuff===========================================

Feel free to use this map for whatever purposes you want but please provide credit
and if possible a link to my stuff.


