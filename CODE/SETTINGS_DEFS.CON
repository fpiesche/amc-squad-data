/*
-------------------------------------------------------------------------------
===============================================================================

         -ooo:             +oo-            `+oo.       ./+oooooooooo+/:
        /NMMMN+            NMMNo`         -dMMM/      .mMMNmmmmmmmmmMMNo
       /NMd:mMMo           NMMNMd:      `oNMNMM/      :MMM/.........mMMh
      oNMd. -mMMy`         NMN:mMNs`   -hMm/mMM/      :MMM:         ::-.
    `sMMh`   .dMMh`        NMN .sNMd:`oNMh. mMM/      :MMM:
   `yMMMdhhhhhmMMMd.       NMN   /mMNdMm+`  mMM/      :MMM:         ss+:
  `hMMdhhhhhhhhhmMMm-      NMN    .yMMh.    mMM/      :MMM/.........mMMh
 .dMMs`         .hMMm:     NMN      :/`     mMM/      .mMMNmmmmmmmmmMMNo
 :oo+            `+oo+     +o+              +oo.       ./++ooooooooo+/:

###############################################################################
+ The AMC Squad
+ CON code by James Stanfield, Cedric "Sangman" Haegeman, Dino Bollinger,
+              Mikko Sandt, Cedric "Zaxtor" Lutes and Dan "Danarama" Gaskill
-------------------------------------------------------------------------------
* See AMC_MAIN.CON for a full list of script authors.

Feel free to use any code in the game for your own uses; just make
sure to mention the authors and/or "The AMC Squad" in your credits.
--------------------------------------------------------------------------------
NOTES:
This file contains the default settings that are used by the game.
--------------------------------------------------------------------------------
*/

// do not alter this
define GV_OPTION_FLAGS 0x20400

// default values
// GAMEPLAY OPTIONS
define DEFAULT_PHOTOSENSITIVITY_MODE NO // Set to YES to enable photosensitivity mode, disabling all screen flashing
define DEFAULT_FACEHUGGERS NO // Set to YES to disable Protozoid slimers and other face-hugging creatures
define DEFAULT_OLD_ITEM_SEL NO // Will revert to the original left/right inventory select if set to yes
define DEFAULT_SLOWMO_FREQUENCY 4 // frequency level of the random slowmo effect -- 4 equals 3.1%, 0 is never, powers of 2
define DEFAULT_QTE NO // Set to YES to disable QTE events like Cultist shotgun struggle
define DEFAULT_SPIDERS NO // Set to YES to disable Spiders
define DEFAULT_STREAMER_MODE NO // Set to YES to enable streamer mode, and disable copy-righted stuff
define DEFAULT_PROMPT_POSITION NO // Set to YES to move use prompts to bottom of screen
// USERMAP OPTIONS
define DEFAULT_RANDOM_ENEMY NO // Will replace Cycloid enemies in usermaps with random monsters of similar size
define DEFAULT_DYNAMIC_MUSIC NO // will play dynamic music in usermaps
// GRAPHIC OPTIONS
define DEFAULT_SHOW_HELMET YES // Disable to remove helmet graphic from character HUDs
define DEFAULT_GLASS_EFFECTS YES // Set to NO to disable glass effects on weapons, HUDs, .etc
define DEFAULT_SHOW_SUBTITLES YES // Will display subtitles during cutscenes
define DEFAULT_SPEECH_BUBBLE YES // Set to NO to disable speech bubbles over talkable NPCs
define DEFAULT_BIG_CURSOR NO // Set to YES to make GUI cursor twice as big
define DEFAULT_VIS_CURSOR NO // Set to YES to make GUI cursor bright neon green
define DEFAULT_HEALTHBAR_TOGGLE 4 // 0 - disabled, 1 - basic, 2 - basic with name,  3 - fancy, 4 - fancy with name
// AUDIO OPTIONS
define DEFAULT_PROTECTED_DING YES // Will play a distinctive Noise if the enemy you're hitting is resistant to that attack
define DEFAULT_VOICE_CD_LENGTH 600 // how long game will wait till playing another character voice, 30 = 1 second
// PERFORMANCE SETTINGS (Adjust these to improve FPS in certain areas)

define DEFAULT_WEAPON_GLOW YES // Set to NO to disable dynamic glow effect on weapons
define DEFAULT_GOLD_SPARKLE YES // Set to NO to disable sparkle effect on gold weapons
define DEFAULT_DISABLE_RAINSNOW NO // Set to YES to disable Rain and SNOW effects
define DEFAULT_ADVANCED_FLAME YES // Set to NO to disable advanced flame effect on torches .etc

define DEFAULT_HIGHRES_CLOUDS YES // Set to NO to enable lo-res clouds for Sky-ship level, set to OFF to disable completely
define DEFAULT_PARTICLE_DISTANCE 65536 // how close player must be to spawn particles like flames .etc default is 65536
define DEFAULT_GLASS_SHARD_TOGGLE YES // whether glass shards drop on the ground and remain there


// index defines
define IDX_PHOTOSENSITIVITY_MODE 0
define IDX_NO_FACEHUGGERS 1
define IDX_OLD_ITEM_SEL 2
define IDX_RANDOM_ENEMY 3
define IDX_DYNAMIC_MUSIC 4

define IDX_PROMPT_POSITION 27

define IDX_SHOW_HELMET 5
define IDX_GLASS_EFFECTS 6
define IDX_SHOW_SUBTITLES 7
define IDX_SPEECH_BUBBLE 8
define IDX_BIG_CURSOR 9
define IDX_VIS_CURSOR 10
define IDX_PROTECTED_DING 11
define IDX_VOICE_CD_LENGTH 12
define IDX_HIGH_VIS_ITEMS 13
define IDX_WEAPON_GLOW 14
define IDX_GOLD_SPARKLE 15
define IDX_NO_RAINSNOW 16
define IDX_ADVANCED_FLAME 17

define IDX_HIGHRES_CLOUDS 19
define IDX_PARTICLE_DISTANCE 20
define IDX_HEALTHBAR_TOGGLE 21
define IDX_GLASS_SHARD_TOGGLE 22
define IDX_SLOWMO_FREQUENCY 23
define IDX_QTE 24
define IDX_SPIDERS 25
define IDX_STREAMER 26
define IDX_DEFAULT_BUTTON 63

// the config variables
gamevar opt_photosensitivity_mode DEFAULT_PHOTOSENSITIVITY_MODE GV_OPTION_FLAGS
gamevar opt_no_facehuggers DEFAULT_FACEHUGGERS GV_OPTION_FLAGS
gamevar opt_old_item_sel DEFAULT_OLD_ITEM_SEL GV_OPTION_FLAGS
gamevar opt_random_enemy DEFAULT_RANDOM_ENEMY GV_OPTION_FLAGS
gamevar opt_dynamic_music DEFAULT_DYNAMIC_MUSIC GV_OPTION_FLAGS

gamevar opt_prompt_position DEFAULT_PROMPT_POSITION GV_OPTION_FLAGS
gamevar opt_high_vis_items NO GV_OPTION_FLAGS

gamevar opt_show_helmet DEFAULT_SHOW_HELMET GV_OPTION_FLAGS
gamevar opt_glass_effects DEFAULT_GLASS_EFFECTS GV_OPTION_FLAGS
gamevar opt_show_subtitles DEFAULT_SHOW_SUBTITLES GV_OPTION_FLAGS
gamevar opt_speech_bubble DEFAULT_SPEECH_BUBBLE GV_OPTION_FLAGS
gamevar opt_big_cursor DEFAULT_BIG_CURSOR GV_OPTION_FLAGS
gamevar opt_vis_cursor DEFAULT_VIS_CURSOR GV_OPTION_FLAGS
gamevar opt_protected_ding DEFAULT_PROTECTED_DING GV_OPTION_FLAGS
gamevar opt_voice_cd_length DEFAULT_VOICE_CD_LENGTH GV_OPTION_FLAGS


gamevar opt_weapon_glow DEFAULT_WEAPON_GLOW GV_OPTION_FLAGS
gamevar opt_gold_sparkle DEFAULT_GOLD_SPARKLE GV_OPTION_FLAGS
gamevar opt_disable_rainsnow DEFAULT_DISABLE_RAINSNOW GV_OPTION_FLAGS
gamevar opt_advanced_flame DEFAULT_ADVANCED_FLAME GV_OPTION_FLAGS

gamevar opt_highres_clouds DEFAULT_HIGHRES_CLOUDS GV_OPTION_FLAGS
gamevar opt_particle_distance DEFAULT_PARTICLE_DISTANCE GV_OPTION_FLAGS
gamevar opt_boss_healthbar_toggle DEFAULT_HEALTHBAR_TOGGLE GV_OPTION_FLAGS
gamevar opt_glass_shard_toggle DEFAULT_GLASS_SHARD_TOGGLE GV_OPTION_FLAGS
gamevar opt_slomo_frequency DEFAULT_SLOWMO_FREQUENCY GV_OPTION_FLAGS
gamevar opt_qte_disable DEFAULT_QTE GV_OPTION_FLAGS
gamevar opt_spiders_disable DEFAULT_SPIDERS GV_OPTION_FLAGS
gamevar opt_streamer_mode DEFAULT_STREAMER_MODE GV_OPTION_FLAGS
