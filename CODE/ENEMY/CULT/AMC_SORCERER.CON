/*
-------------------------------------------------------------------------------
===============================================================================

         -ooo:             +oo-            `+oo.       ./+oooooooooo+/:
        /NMMMN+            NMMNo`         -dMMM/      .mMMNmmmmmmmmmMMNo
       /NMd:mMMo           NMMNMd:      `oNMNMM/      :MMM/.........mMMh
      oNMd. -mMMy`         NMN:mMNs`   -hMm/mMM/      :MMM:         ::-.
    `sMMh`   .dMMh`        NMN .sNMd:`oNMh. mMM/      :MMM:
   `yMMMdhhhhhmMMMd.       NMN   /mMNdMm+`  mMM/      :MMM:         ss+:
  `hMMdhhhhhhhhhmMMm-      NMN    .yMMh.    mMM/      :MMM/.........mMMh
 .dMMs`         .hMMm:     NMN      :/`     mMM/      .mMMNmmmmmmmmmMMNo
 :oo+            `+oo+     +o+              +oo.       ./++ooooooooo+/:

###############################################################################
+ The AMC Squad
+ CON code by James Stanfield, Cedric "Sangman" Haegeman, Dino Bollinger,
+              Mikko Sandt, Cedric "Zaxtor" Lutes and Dan "Danarama" Gaskill
-------------------------------------------------------------------------------
* See AMC_MAIN.CON for a full list of script authors.

Feel free to use any code in the game for your own uses; just make
sure to mention the authors and/or "The AMC Squad" in your credits.
--------------------------------------------------------------------------------
NOTES:

--------------------------------------------------------------------------------
*/

damageeventtile SORCERER

action A_SORCERER_FROZEN 5 1 5 1 10
action A_SORCERER_FLOAT 0 1 5 1 10
action A_SORCERER_FLOATUP 0 1 5 1 10
action A_SORCERER_FLOATDOWN 0 1 5 1 10
action A_SORCERER_CAST 10 3 5 1 20
action A_SORCERER_PUSH 10 3 5 1 20
action A_SORCERER_PAIN 5 1 5 1 10
action A_SORCERER_TELEPORT 5 1 5 1 10
action A_SORCERER_DIE 25 8 1 1 15

move M_SORCERER_FLOAT 64 0
move M_SORCERER_FLOATUP 64 -64
move M_SORCERER_FLOATDOWN 64 64

useractor enemy SORCERER 250

	ifaction 0
	{
		geta[].owner myowner
		set actor_type TYPE_SUPERNATURAL
		set blood_type 1
		set MONSTER_FLAGS 1
		state monst_glow
		ifspritepal 0 spritepal 21
		geta[].pal PALSAVED
		spawn SHOOTME
		spriteflags ROBOT_ENEMY
		sound PRIEST_RECOG

		strength 250
		sizeat 24 24
		action A_SORCERER_FLOAT
		cstat 257
	}

	ifg enemy_cooldown1 0 sub enemy_cooldown1 1

	ifg silence_damage 0
	{
		sub silence_damage 1
		state spawn_curse_particles
	}

	ifg ALLY_VOICE 0 sub ALLY_VOICE 1

	state check_canfloat

	ifaction A_SORCERER_FROZEN { fall state frozen_code }
	else ifaction A_SORCERER_DIE
	{
		move STOP

		ifactioncount 1 cstat 2
		seta[].blend 255
		add INTERNALCOUNT 1
		spriteflags 7

		ife INTERNALCOUNT 1 { espawn 8433 geta[RETURN].z temp sub temp 4096 seta[RETURN].z temp }
		ife INTERNALCOUNT 10 { spritepal 1 spawn BIG_BLOOD_EXE state standard_jibs spritepal 0 }
		seta[].shade -64
		ifactioncount 7 fall
		ifactioncount 8 killit
	}
	else ifaction A_SORCERER_PAIN
	{
		sub PAIN_AMOUNT 1
		move STOP
		ifl PAIN_AMOUNT 1 action A_SORCERER_FLOAT
	}
	else ifaction A_SORCERER_FLOAT
	{
		ifmove M_SORCERER_FLOAT nullop
		else move M_SORCERER_FLOAT geth dodgebullet

		state checkfortarget

		ifn my_target -1
		{
			ifrnd 4
			{
				ife ALLY_VOICE 0 { sound PRIEST_CAST01 set ALLY_VOICE 300 }
				sound PRI_RDY
				action A_SORCERER_CAST
			}
			else
			{
				dist xydist THISACTOR my_target

				ife my_target PLAYER_IDENTITY
				 ifl xydist 1024
				 ifrnd 32
					action A_SORCERER_PUSH
				else ifg xydist 8098
				      ifl xydist 16384
				       ifrnd 16
				        ife camerasprite -1
				         ife silence_damage 0
							{
								getp[].posx temp
								getp[].posy temp2
								set temp3 temp
								add temp3 1024
								randvar temp4 2048
								rotatepoint temp temp2 temp3 temp2 temp4 temp5 temp6
								updatesector temp5 temp6 temp7
								// if the player has the Enemy tracking protection mystic research, teleport has a 20% chance of failing
								ife MYSTICAL_RESEARCH[RS_M_SHADE] 2 ifrnd 50 set temp7 -1
								else
								ifn temp7 -1
								{
									set INTERNALCOUNT 0
									action A_SORCERER_TELEPORT
									resetcount
								}
							}
				else ifrnd 128
				{
					state check_canfloat
					// Skip the float detection code if the wizard can't float freely in here
					ife temp9 1
					{
						geta[my_target].z z
						geta[].z my_z
						ifg my_z z
						{
							action A_SORCERER_FLOATUP
						}
						else
						{
							ifg temp10 my_z
								action A_SORCERER_FLOATDOWN
						}
					}
				}
			}
		}
	}
	else ifaction A_SORCERER_TELEPORT
	{
		move STOP spin

		add INTERNALCOUNT 1
		spawn FRAMEEFFECT1
		ife INTERNALCOUNT 4 { cstat 2 soundonce PRIEST_CAST02 sizeto 2 24 }
		ife INTERNALCOUNT 26
		{
			cstat 32768
			getp[].posx temp
			getp[].posy temp2
			set temp3 temp
			add temp3 1024
			randvar temp4 2048
			rotatepoint temp temp2 temp3 temp2 temp4 temp5 temp6
			updatesectorz temp5 temp6 player[].posz temp7
			ifn temp7 -1
			{
				spawn 8433
				// It's possible that this teleport code will put the sorcerer in the ceiling or floor, but that will be corrected by the check_canfloat state
				seta[].x temp5
				seta[].y temp6
				getp[].posz temp8
				sub temp8 1754
				seta[].z temp8
			}
			else
			{
				cstat 257
				sizeat 24 24
				action A_SORCERER_FLOAT
			}
		}
		ife INTERNALCOUNT 27 { soundonce PRIEST_APPEAR cstat 2 sizeto 24 24 spawn 8433 }
		ife INTERNALCOUNT 40 { spawn SHOOTME cstat 257 sizeat 24 24 action A_SORCERER_FLOAT }
	}
	else ifaction A_SORCERER_FLOATUP
	{
		ifmove M_SORCERER_FLOATUP nullop
		else move M_SORCERER_FLOATUP geth getv dodgebullet

		state thisactor_getzrange
		state checkfortarget

		ifn my_target -1
		{
			ifrnd 4
			{
				sound PRI_RDY
				action A_SORCERER_CAST
			}
			else
			{
				dist xydist THISACTOR my_target

				ife my_target PLAYER_IDENTITY
				  ifl xydist 1024
				  ifrnd 32
					action A_SORCERER_PUSH
				else ifrnd 16
					action A_SORCERER_FLOAT

			}
			else
				action A_SORCERER_FLOAT
		}
		else
			action A_SORCERER_FLOAT
	}
	else ifaction A_SORCERER_FLOATDOWN
	{
		ifmove M_SORCERER_FLOATDOWN nullop
		else move M_SORCERER_FLOATDOWN geth getv dodgebullet

		state thisactor_getzrange
		state checkfortarget

		ifn my_target -1
		{
			ifrnd 4
			{
				sound PRI_RDY
				action A_SORCERER_CAST
			}
			else
			{
				dist xydist THISACTOR my_target

				ife my_target PLAYER_IDENTITY
				  ifl xydist 1024
				  ifrnd 32
					action A_SORCERER_PUSH
				else ifrnd 16
					action A_SORCERER_FLOAT
			}
		}

		// temp10: Minimal floating distance for this sector
		ifg sprite[].z temp10
		{
			seta[].z temp10
			action A_SORCERER_FLOAT
		}
	}
	else ifaction A_SORCERER_CAST
	{
		move STOP

		ife my_target -1
			action A_SORCERER_FLOAT
		else
		{
			state fronttowardstarget

			spriteflags 7
			seta[].shade -64

			ifactioncount 3
			{
				set PROJECTILE_TO_SHOOT PRIEST_BLAST
				set PROJECTILE_FIRING_SOUND PRI_FIRE
				state sang_enemyShootProjectile
				setthisprojectile[RETURN].extra 140

				action A_SORCERER_FLOAT
			}
		}
	}

	// Only on player!
	else ifaction A_SORCERER_PUSH
	{
		move STOP
		set my_target PLAYER_IDENTITY
		state fronttowardstarget

		spriteflags 7
		seta[].shade -64
		ifactioncount 1
		{
			getp[].posx mx
			getp[].posy my
			geta[].x x
			geta[].y y
			sub mx x
			sub my y
			getangle knockbackang mx my
			set knockback 6
			sound FORCE_BLAST
			ifpdistl 1024
			{
				wackplayer
				palfrom 20 30 0 0
				addphealth -15
			}
			action A_SORCERER_FLOAT
			resetactioncount
		}
	}

	ifhitweapon
	{
		state NEWGUNEFFECTS
		soundonce PRIEST_PAIN
		espawn BLOOD seta[RETURN].pal 1
		ifwasweapon VOID_PLAYER_BOLT { sound EN_CURSE set silence_damage 150 }
		ifwasweapon VOID_BOLT { sound EN_CURSE set silence_damage 300 }
		ifwasweapon VOID_BLAST { sound EN_CURSE set silence_damage 900 }
		spritepal 1
		guts JIBS6 1
		state random_wall_jibs
		getlastpal
		ifwasweapon 6875 { set WEP3_BLOODY 3 sound STAB_01 }
		ifwasweapon RPG
		{ seta[].xvel 5 }
		ifdead
		{
			// NMIND sorcerers spawn a mirror orb if player doesn't have one
			ife CHAR 4
			  ife sprite[myowner].picnum NMIND
			{
				getp[].holoduke_amount temp

				ifle temp 0
					spawn HOLODUKE
				else ifrnd 64
					spawn GROWAMMO
			}

			ifg ice_damage 0 { spritepal 1 strength 1 sound SOMETHINGFROZE action A_SORCERER_FROZEN }
			else
			{
				state jib_sounds
				sound PRIEST_DEATH
				action A_SORCERER_DIE
				state rf
				addkills 1

				set INTERNALCOUNT 0
			}
		}
		else ifpdistl 1024 ife enemy_cooldown1 0 { ifaction A_SORCERER_PUSH nullop else action A_SORCERER_PUSH set enemy_cooldown1 150 }
		else ifg PAIN_AMOUNT 0 action A_SORCERER_PAIN
		else ifrnd 16 { state PAIN_SKILL_LEVELADJUST action A_SORCERER_PAIN }
	}

	state ENEMYKNOCKBACKS

enda


// DUMMY FOR SANG CAPTURE CUTSCENE

action DUMMY -1 1 5 1 1

action DUMMYCAST 10 3 5 1 32

useractor notenemy 9773
ifaction 0 action DUMMY

ifn camerasprite -1
ifaction DUMMY
	{
	checkactivatormotion HITAGSAVED

		ife RETURN 1
			{
			seta[].shade 0
			sound PRIEST_CAST01
			action DUMMYCAST
			}

	}

ifaction DUMMYCAST
	{
	ifactioncount 3
		{
		globalsound WHOOSH
		palfrom 62 62 62 62
		killit
		}
	}

enda

useractor notenemy 9777
ifspritepal 3 { ifn camerasprite -1 cstat 0 else cstat 32768  }
action FIVE_ANG
enda
