/*
-------------------------------------------------------------------------------
===============================================================================

         -ooo:             +oo-            `+oo.       ./+oooooooooo+/:
        /NMMMN+            NMMNo`         -dMMM/      .mMMNmmmmmmmmmMMNo
       /NMd:mMMo           NMMNMd:      `oNMNMM/      :MMM/.........mMMh
      oNMd. -mMMy`         NMN:mMNs`   -hMm/mMM/      :MMM:         ::-.
    `sMMh`   .dMMh`        NMN .sNMd:`oNMh. mMM/      :MMM:
   `yMMMdhhhhhmMMMd.       NMN   /mMNdMm+`  mMM/      :MMM:         ss+:
  `hMMdhhhhhhhhhmMMm-      NMN    .yMMh.    mMM/      :MMM/.........mMMh
 .dMMs`         .hMMm:     NMN      :/`     mMM/      .mMMNmmmmmmmmmMMNo
 :oo+            `+oo+     +o+              +oo.       ./++ooooooooo+/:

###############################################################################
+ The AMC Squad
+ CON code by James Stanfield, Cedric "Sangman" Haegeman, Dino Bollinger,
+              Mikko Sandt, Cedric "Zaxtor" Lutes and Dan "Danarama" Gaskill
-------------------------------------------------------------------------------
* See AMC_MAIN.CON for a full list of script authors.

Feel free to use any code in the game for your own uses; just make
sure to mention the authors and/or "The AMC Squad" in your credits.
--------------------------------------------------------------------------------
NOTES:

--------------------------------------------------------------------------------
*/

damageeventtile CYBERDEMON

action A_CYBERD_IDLE -5 1 5 1 20
action A_CYBERD_DEAD 36 1 1 1 20
action A_CYBERD_DYING 30 6 1 1 20
action A_CYBERD_SEEK 0 4 5 1 15
action A_CYBERD_FIRE 20 2 5 1 30
action A_CYBERD_PREPARE 20 1 5 1 10

action A_CYBERD_ARM 91 1 1
action A_CYBERD_THROW 92 5 1 1 16
action A_CYBERD_EXP 98

move M_CYBERD_WALK 160

useractor enemystayput CYBERDEMONSTAYPUT 6000
cactor CYBERDEMON
enda

useractor enemy CYBERDEMON 6000
	fall

	ifaction 0
	{
		cstator 257
		strength 6000
		sizeat 40 40
		set actor_type TYPE_CASE_SPECIFIC
		state monst_glow
		spawn SHOOTME

		clipdist 96
		spriteflags NORMAL_ENEMY
		ifspritepal 3
			{
			set CURBOSS THISACTOR
			set BOSS_TYPE 2
			}

		action A_CYBERD_IDLE

	}

	// crush player if they get too close
	// this doesn't happen for other targets atm
	ifdead nullop
	else ifpdistl 1526
	  ifcansee
	{
	ife CHAR 2 ifand CHAR_APP 4 nullop else
		{
		wackplayer
		palfrom 40 40 0 0
		addphealth -100
		}
	}


	ifaction A_CYBERD_IDLE
	{
		move STOP
		state checkfortarget

		ifn my_target -1
		{
			globalsound CYBERDEMON_SEE
			action A_CYBERD_SEEK
		}
	}
	else ifaction A_CYBERD_EXP
		{
			move STOP

			add INTERNALCOUNT 1
			ife INTERNALCOUNT 1 sound CYBERDEMON_DIE
			ife INTERNALCOUNT 20 { spawn NUCLEAR_EXPLOSION globalsound CHOPPER_EXPLODE }
			ife INTERNALCOUNT 30
			{
				flash
				sound HUGE_GIB
				shoot SPARK2
				shoot SPARK2
				shoot SPARK2
				shoot SPARK2
				shoot SPARK2
				shoot SPARK2
				spawn BIG_BLOOD_EXE
				spritepal 27
				ife INVASION 1 nullop
				else
				{
					state standard_jibs
					state standard_jibs
					state standard_jibs
					state monst_body_jibs
					state monst_body_jibs
				}
				debris SCRAP1 14
				guts JIBS6 2
				killit
			}
		}
	else ifaction A_CYBERD_THROW
		{
		move STOP faceplayersmart
		add INTERNALCOUNT 1
		ife INTERNALCOUNT 16 { sound SPELL4FIRE shoot SPELL4 }
		ife INTERNALCOUNT 20 set INTERNALCOUNT 0
		}	
	else ifaction A_CYBERD_ARM
		{
		move STOP
		ifactioncount 1 action A_CYBERD_THROW
		}
	else ifaction A_CYBERD_DEAD
	{
		move STOP
	}
	else ifaction A_CYBERD_DYING
	{
		move STOP

		add INTERNALCOUNT 1
		ifactioncount 6 action A_CYBERD_DEAD
		ife INTERNALCOUNT 1 sound CYBERDEMON_DIE
		ife INTERNALCOUNT 20 { spawn NUCLEAR_EXPLOSION globalsound CHOPPER_EXPLODE }
		ife INTERNALCOUNT 30
		{
			flash
			sound HUGE_GIB
			shoot SPARK
			shoot SPARK
			shoot SPARK
			shoot SPARK
			shoot SPARK
			shoot SPARK
			spawn BIG_BLOOD_EXE
			spritepal 27
			ife INVASION 1 nullop
			else
			{
				state standard_jibs
				state standard_jibs
				state standard_jibs
				state monst_body_jibs
				state monst_body_jibs
			}
			debris SCRAP1 14
			guts JIBS6 2
			getlastpal
		}
	}
	else ifaction A_CYBERD_SEEK
	{
		ifmove M_CYBERD_WALK nullop
		else move M_CYBERD_WALK geth

		ifcount 6 { ifpdistl 16000 quake 13 ifactorsound THISACTOR CYBERDEMON_STOMP nullop else sound CYBERDEMON_STOMP }
		ifcount 13 { ifactorsound THISACTOR CYBERDEMON_METAL nullop else sound CYBERDEMON_METAL resetcount }

		// If we already have a target and we're too close to it, walk away from it first
		set temp2 0 // set to 1 if we can call checkfortarget regularly
		ifn my_target -1
		{
			dist temp THISACTOR my_target
			ifle temp 1024
			{
				// angle to actor
				set ANGLE_TO_ACTOR my_target
				state angleTo

				// walk back
				rand temp 512
				add temp 512
				add angvar temp
				seta[].ang angvar
			}
			else
				set temp2 1

		}
		else
			set temp2 1

		ife temp2 1
		{
			state checkfortarget

			ifn my_target -1
			{
				dist temp THISACTOR my_target

				ifg temp 1024
				{
					state SKILL_SHOOT_LEVELADJUST
					ifl RANDOM_CHANCE SKILLCHANCE
						action A_CYBERD_PREPARE
				}
				else
				{
					// walk back
					geta[].ang angvar
					add angvar 1024
					seta[].ang angvar
				}
			}
		}
	}
	else ifaction A_CYBERD_FIRE
	{
		ife my_target PLAYER_IDENTITY
			move STOP faceplayersmart
		else
			{
			move STOP
			state fronttowardstarget
			}

		ifactioncount 2
		{
			set CALCZ_TARGET_OFFSET -9500
			set PROJECTILE_TO_SHOOT RPG
			set PROJECTILE_FIRING_SOUND CYBERDEMON_SHOOT
			set PROJECTILE_DISABLEAUTOAIM 1
			state sang_enemyShootProjectile

			geta[].x temp
			geta[].y temp2

			set temp3 temp
			sub temp3 256

			rotatepoint temp temp2 temp3 temp2 temp4 temp5 temp6

			seta[RETURN].x temp5
			seta[RETURN].y temp6

			flash
			ifrnd 128 resetactioncount
			else action A_CYBERD_SEEK
		}
	}
	else ifaction A_CYBERD_PREPARE
	{
		ife my_target PLAYER_IDENTITY
			move STOP faceplayersmart
		else
			move STOP

		ifactioncount 3
			action A_CYBERD_FIRE
	}

	ifhitweapon
	{
		ifspritepal 3 set CURBOSS THISACTOR
		ifrnd 8 soundonce BRUISER_PAIN
		ifwasweapon RPG seta[].xvel 0
		ifwasweapon RADIUSEXPLOSION seta[].xvel 0
		state NEWGUNEFFECTS
		//spawn BLOOD
		guts JIBS6 1
		state random_wall_jibs
		ifdead
		{
			set INTERNALCOUNT 0
			add TREASURE_FOUND 5
			addkills 1
			state rf

			ifn HITAGSAVED 0
			{
			   operateactivators HITAGSAVED THISACTOR
			   operatemasterswitches HITAGSAVED
			   operaterespawns HITAGSAVED
			}
			ifaction A_CYBERD_ARM action A_CYBERD_EXP else
			ifaction A_CYBERD_THROW action A_CYBERD_EXP else
			action A_CYBERD_DYING
		}
		else ifaction A_CYBERD_IDLE
		{
			// point towards the projectile owner
			geta[].htowner temp
			ifn temp -1
			{
				getav[temp].faction_flag temp2

				ifg temp2 0
				{
					ifand temp2 faction_flag
						nullop
					else
						set my_target temp
				}
			}

			action A_CYBERD_SEEK
		}
		else
			ifspritepal 3 ifstrength 1000
			{
			ifaction A_CYBERD_ARM nullop else
			ifaction A_CYBERD_THROW nullop else
			ifaction A_CYBERD_EXP nullop else
				{
				spawn BIG_EXPLOSION 
				debris SCRAP1 2
				sound EXPLOSION_BIG
				debris SCRAP2 2
				sound VENT_BUST action A_CYBERD_ARM 
				}
			} 
	}
enda
