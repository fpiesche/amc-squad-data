/*
-------------------------------------------------------------------------------
===============================================================================

         -ooo:             +oo-            `+oo.       ./+oooooooooo+/:
        /NMMMN+            NMMNo`         -dMMM/      .mMMNmmmmmmmmmMMNo
       /NMd:mMMo           NMMNMd:      `oNMNMM/      :MMM/.........mMMh
      oNMd. -mMMy`         NMN:mMNs`   -hMm/mMM/      :MMM:         ::-.
    `sMMh`   .dMMh`        NMN .sNMd:`oNMh. mMM/      :MMM:
   `yMMMdhhhhhmMMMd.       NMN   /mMNdMm+`  mMM/      :MMM:         ss+:
  `hMMdhhhhhhhhhmMMm-      NMN    .yMMh.    mMM/      :MMM/.........mMMh
 .dMMs`         .hMMm:     NMN      :/`     mMM/      .mMMNmmmmmmmmmMMNo
 :oo+            `+oo+     +o+              +oo.       ./++ooooooooo+/:

###############################################################################
+ The AMC Squad
+ CON code by James Stanfield, Cedric "Sangman" Haegeman, Dino Bollinger,
+              Mikko Sandt, Cedric "Zaxtor" Lutes and Dan "Danarama" Gaskill
-------------------------------------------------------------------------------
* See AMC_MAIN.CON for a full list of script authors.

Feel free to use any code in the game for your own uses; just make
sure to mention the authors and/or "The AMC Squad" in your credits.
--------------------------------------------------------------------------------
NOTES:

--------------------------------------------------------------------------------
*/

spriteflags BRUISER SFLAG_NODAMAGEPUSH

damageeventtile BRUISER

action A_BRUISER_START 0 1 5 1 20
action A_BRUISER_IDLE  0 1 5 1 20
action A_BRUISER_DYING 55 10 1 1 15
action A_BRUISER_FREEZE 66
action A_BRUISER_PAIN 50 1 5 1 10
action A_BRUISER_SEEK 0  4 5 1 16
action A_BRUISER_FIRE 20 3 5 1 25
action A_BRUISER_FIREBIG 35 3 5 1 15
action A_BRUISER_SCRATCH  20 3 5 1 25
action A_BRUISER_RUN      0  4 5 1 12

move M_BRUISER_WALK 80
move M_BRUISER_RUN 140



useractor enemystayput BRUISERSTAYPUT 1250 A_BRUISER_START
cactor BRUISER
enda

spritenoshade BRUISER

useractor enemy BRUISER 1250 A_BRUISER_START
	fall

	gets[].floorshade temp
	sub temp 10
	ifl temp -127 set temp -127
	seta[].shade temp

	ifaction A_BRUISER_START
	{
	ife actor_pal 0 geta[].pal actor_pal
	cstator 257
	ife actor_pal 24
		{
			spritepal 0
			strength 2000
			sizeat 36 36
			set CURBOSS THISACTOR
			set BOSS_TYPE 2

			// bigger guy so need to aim a little bit lower to be able to hit most targets
			set CALCZ_TARGET_OFFSET -5000
		}
		else {
			strength 1250
			sizeat 28 28
		}

		ifspawnedby NSOUL_SPAWNER
			strength 250

		state monst_glow
		espawn SHOOTME seta[RETURN].pal 1
		action A_BRUISER_IDLE
	}

	state enemy_ice_damage
	state enemy_spirit_damage

	ifg ice_damage 0 ifg sprite[].extra 15 addstrength -15

	ifaction A_BRUISER_IDLE
	{
		move STOP
		state checkfortarget
		ifn my_target -1
		{
			sound BRUISER_ALERT
			action A_BRUISER_SEEK
		}
	}
	else ifaction A_BRUISER_DYING
	{
		move STOP spin
		spawn 3296
		strength 0
		ifactioncount 5
		{
			spritepal 106
			spawn GROUNDEXPLOSION flash
			quake 13
			sound BARREL_EXPLODE
			state standard_jibs
			state monst_body_jibs
			state monst_body_jibs
			killit
		}
	}
	else ifaction A_BRUISER_FREEZE
		{
		state frozen_code
		spritepal 0
		}
	else ifaction A_BRUISER_PAIN
	{
		move STOP
		sub PAIN_AMOUNT 2
		ifl PAIN_AMOUNT 1
			action A_BRUISER_FIRE
	}
	else ifaction A_BRUISER_SEEK
	{
		ifmove M_BRUISER_WALK nullop
		else move M_BRUISER_WALK geth

		state checkfortarget

		ifn my_target -1
		{
			dist temp THISACTOR my_target
			ifl temp 1024 action A_BRUISER_SCRATCH
			else ifl temp 8192 ifrnd 16 action A_BRUISER_RUN
			else ifrnd 76
			{
				state SKILL_SHOOT_LEVELADJUST
				div SKILLCHANCE 2
				ifl RANDOM_CHANCE SKILLCHANCE ife silence_damage 0 action A_BRUISER_FIREBIG
				else action A_BRUISER_FIRE
			}
		}
	}
	else ifaction A_BRUISER_FIRE
	{
		move STOP

		state checkfortarget

		ife my_target -1
			action A_BRUISER_SEEK
		else
		{
			state fronttowardstarget

			ifpdistlvar opt_particle_distance // if player is within defined distance, spawn extra flare effects
			{
				espawn PROJECTILE_FLARE
				geta[].htg_t 0 temp2
				set temp3 sprite[].x
				ifg temp2 11 add temp3 64
				else ifg temp2 5 add temp3 256
				else add temp3 300
				geta[].ang temp4
				add temp4 512
				rotatepoint sprite[].x sprite[].y temp3 sprite[].y temp4 temp5 temp6
				seta[RETURN].x temp5
				seta[RETURN].y temp6
				seta[RETURN].ang sprite[].ang
				geta[].z temp4
				ifg temp2 11 sub temp4 8062
				else ifg temp2 5 sub temp4 12784
				else sub temp4 14784
				seta[RETURN].z temp4
			}

			ifactioncount 3
			{
				set PROJECTILE_TO_SHOOT BRUISERFIREBALL
				set PROJECTILE_FIRING_SOUND BRUISER_THROW
				state sang_enemyShootProjectile
				action A_BRUISER_SEEK
			}
		}
	}
	else ifaction A_BRUISER_FIREBIG
	{
		move STOP

		state checkfortarget

		ife my_target -1
			action A_BRUISER_SEEK
		else
		{
			ifpdistlvar opt_particle_distance // if player is within defined distance, spawn extra flare effects
			{
				espawn PROJECTILE_FLARE
				geta[].htg_t 0 temp2
				set temp3 sprite[].x
				ifg temp2 7 add temp3 64
				else ifg temp2 3 add temp3 300
				else add temp3 350
				geta[].ang temp4
				add temp4 512
				rotatepoint sprite[].x sprite[].y temp3 sprite[].y temp4 temp5 temp6
				seta[RETURN].x temp5
				seta[RETURN].y temp6
				seta[RETURN].ang sprite[].ang
				geta[].z temp4
				ifg temp2 7 sub temp4 8062
				else ifg temp2 3 sub temp4 12784
				else sub temp4 14784
				seta[RETURN].z temp4

				espawn PROJECTILE_FLARE
				set temp3 sprite[].x
				ifg temp2 7 add temp3 64
				else ifg temp2 3 add temp3 300
				else add temp3 350
				geta[].ang temp4
				add temp4 -512
				rotatepoint sprite[].x sprite[].y temp3 sprite[].y temp4 temp5 temp6
				seta[RETURN].x temp5
				seta[RETURN].y temp6
				seta[RETURN].ang sprite[].ang
				geta[].z temp4
				ifg temp2 7 sub temp4 8062
				else ifg temp2 3 sub temp4 12784
				else sub temp4 14784
				seta[RETURN].z temp4
			}

			ifactioncount 3
			{
				flash
				sound BRUISER_THROW
				sound BRUISER_THROW
				sound BRUISER_THROW

				set PROJECTILE_FIRING_SOUND BRUISER_THROW
				set PROJECTILE_TO_SHOOT BRUISERFIREBALL
				state sang_enemyShootProjectile

				state sang_enemyShootProjectile

				state sang_enemyShootProjectile
				geta[RETURN].ang temp
				add temp 64
				seta[RETURN].ang temp

				state sang_enemyShootProjectile
				geta[RETURN].ang temp
				sub temp 64
				seta[RETURN].ang temp

				action A_BRUISER_SEEK
			}
		}
	}
	else ifaction A_BRUISER_SCRATCH
	{
		move STOP

		state checkfortarget

		ife my_target -1
			action A_BRUISER_SEEK
		else
		{
			ldist temp THISACTOR my_target
			ifg temp 1536 action A_BRUISER_RUN
			else state fronttowardstarget

			soundonce BRUISER_FLAME
			ifactioncount 3
			{
				set PROJECTILE_TO_SHOOT 7600
				set PROJECTILE_FIRING_SOUND -1
				state sang_enemyShootProjectile
				resetactioncount
			}
		}
	}
	else ifaction A_BRUISER_RUN
	{
		ifmove M_BRUISER_RUN nullop
		else move M_BRUISER_RUN geth

		ifrnd 8 spawn BURNING2

		state checkfortarget

		ifn my_target -1
		{
			state fronttowardstarget
			dist temp THISACTOR my_target
			ifl temp 1024 action A_BRUISER_SCRATCH
			else ifl temp 8192
			  ifrnd 128
			{
				state SKILL_SHOOT_LEVELADJUST
				ifl RANDOM_CHANCE SKILLCHANCE
					action A_BRUISER_FIREBIG
			}
			else ifg temp 8192
				action A_BRUISER_SEEK
		}
		else
			action A_BRUISER_SEEK
	}

ifg silence_damage 0
	{
	sub silence_damage 1
	state spawn_curse_particles
	}

	ifhitweapon
	{
		ife actor_pal 24 set CURBOSS THISACTOR
		ifn actor_pal 24 state MIA_GUN_STATE

		ifge OWNERSAVED 0
		  ife sprite[OWNERSAVED].picnum NSOUL_SPAWNER
			state NEWGUNEFFECTS

		geta[].htpicnum temp
		ife temp 7163 seta[].htextra -1
		ifwasweapon 7160 { set ice_damage 52 addstrength -100 }
		ifwasweapon 29020 addstrength -200
		ifwasweapon FREEZEBLAST { set ice_damage 26 addstrength -25 }
		ifwasweapon VOID_PLAYER_BOLT
			{
			sound EN_CURSE
			set silence_damage 150
			}
		ifwasweapon VOID_BOLT
			{
			sound EN_CURSE
			ife actor_pal 24 set silence_damage 150 else
			set silence_damage 300
			}
		ifwasweapon VOID_BLAST
			{
			sound EN_CURSE
			ife actor_pal 24 set silence_damage 450 else
			set silence_damage 900
			}
		ifwasweapon RPG seta[].xvel 0
		ifwasweapon RADIUSEXPLOSION seta[].xvel 0
		//spawn BLOOD
		guts JIBS6 1
		state random_wall_jibs

		sound BRUISER_PAIN
		ifdead
		{
			ifg ice_damage 0
			{
				ife actor_pal 3 operateactivators 15 THISACTOR
				ife actor_pal 24 { add TREASURE_FOUND 3 ifand HELLGKEYSA 32 nullop else add HELLGKEYSA 32 }
				spritepal 1
				strength 1
				sound SOMETHINGFROZE
				action A_BRUISER_FREEZE
			}
			else
			{
				ife actor_pal 3 operateactivators 15 THISACTOR
				ife actor_pal 24 { add TREASURE_FOUND 3 ifand HELLGKEYSA 32 nullop else add HELLGKEYSA 32 }
				sound BRUISER_DIE
				state rf
				action A_BRUISER_DYING
			}
		addkills 1
		}
		else ifg PAIN_AMOUNT 0 action A_BRUISER_PAIN
		else ifrnd 16 { state PAIN_SKILL_LEVELADJUST action A_BRUISER_PAIN }
		else ifaction A_BRUISER_IDLE { state PAIN_SKILL_LEVELADJUST action A_BRUISER_PAIN }
	}

enda
