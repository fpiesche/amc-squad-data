"The AMC Squad" - EP4 Credits

At the end of production, this list will be integrated into the main game itself as a full credits map.

!! If your work is in this game and you are not properly credited, please contact James at: jstanf36@gmail.com
!! or reach out to us over ModDB, Discord,  or other online channels.
!! We will rectify these mistakes as soon as possible, with our sincere apologies.

==============================
-- AMC SQUAD Episode 1 to 4 --
==============================

============
Core Team
============

James Stanfield (Project lead, CON coding, mapping, everything)
Sangluss (aka. Sangman) (CON coding, engine tweaks, mapping, bugfixes)
sebabdukeboss20 (art design, mapping)
Michael 'MickyC' Crisp (mapping)
Snowfall (aka. Astra) (art design, mapping)
AliCatGamer (art design, mapping, voice acting, music)
Doom64hunter (CON coding, engine tweaks, art edits, bugfixes)
Eddie Teach (voice acting, beta tester)
HiveBoSs (music)
Thobias 'Loke' Fast (mapping)
Mikko Sandt (mapping, beta testing)
Rhaisher (beta testing, mapping)
Forge (beta testing, concierge)
Major Tom (voice acting, community mascot)


Honorary mentions:
-------
Cedric 'Zaxtor Znort' Lutes (mapping)
Rob 'Highwire' Wijkstra (mapping, past voice acting)
Merlijn Van Oostrum (voice acting)
Maarten Van Oostrum (voice acting)
Rusty Nails (mapping, past voice acting)
Geoffrey (mapping, past voice acting)
turbogurra (sprite work)
Jolteon (sprite work, wiki editing)


Internal Beta Testing
=====
Forge
Mikko Sandt
Rhaisher
Somagu/Sledge
Adamant Archvile
David Wolf
mekanchest


=========================
Donators
=========================
Anonymous
Analmouse
Bunglar
Choatic Akantor
Dino
Dynamo
geecko
J.F CRIMSON
KillerBudgie
Loke
PC
Solais
Xamp
theoracleofllaena
Geedra
Wheaton Adams


=========================
Contributor Credits
=========================

Mapping (Main Campaign Maps)
=====
Cedric 'Zaxtor Znort' Lutes
DavoX
Geoffrey
AliCatGamer
James Stanfield
Michael 'MickyC' Crisp
Mikko Sandt
Rhaisher
Rob 'Highwire' Wijkstra
Rusty Nails
sebabdukeboss20
Sangluss
Snowfall
Thobias 'Loke' Fast


Map authors who donated work to us:
======
Mike Von Skellington
Mr. Sinister
Mike Norvak
DanM
Drek


EDF Missions
=====

ABBA.map                -- "Another Big Base Attacked"                          -- Maarten v. Oostrum
ALLUNDED.map            -- "Alone in the Dark with the Undead"                  -- Juha Maekinen
BCBP.map                -- "Beach Community Build Project"                      -- MickyC, underTaker et al. (Duke4 community project)
CBP8.map                -- "Community Build Project 8: Metropolitan Starlight"  -- MickyC, Norvak et al. (Duke4 community project)
CLCOAST.map             -- "Clear the Coast"                                    -- Merlijn van Oostrum & Maarten van Oostrum
DHFC.map                -- "Driving Home From Christmas"                        -- Rob 'Highwire' Wijkstra
DRACULA.map             -- "Dracula's Castle"                                   -- James Stanfield
EDFBUREAU.map           -- "Insurance Overload (aka EDF Bureau)"                -- Alias Conrad Coldwood
LH1.map to LH6.map      -- "Lost Highway"                                       -- "Petr and Lukas"
HOTELFAN.map            -- "The Fantasy Hotel"                                  -- Blackjack
RED1.map to RED5.map    -- "Red Series"                                         -- Merlijn van Oostrum & Maarten van Oostrum
SECRET_B.map            -- "Secret Base"                                        -- Cedric "Zaxtor Znort" Lutes
TFC01.map to TFC07.map  -- "The Final Confrontation"                            -- 3DVisions (fragmastr)
UNDER_FT.map            -- "Underground Fortress"                               -- Cedric "Zaxtor Znort" Lutes
WCTIME.map              -- "Wonderful Christmas Time"                           -- James Stanfield
WESTATRN.MAP            -- "West Alien Train"                                   -- Cedric "Zaxtor Znort" Lutes
WETD.map                -- "Wet Dream (Including Tropical Alien Massacre)"      -- Rob 'Highwire' Wijkstra

-- for additional information, see their respective READMEs in the folder EDF_USERM/READMEs --


Music Composers
=====
HiveBoss
Gavin 'Xendraven' Fry
TheDavisD
AliCatGamer


Voices (alphabetically)
=====
Alicatgamer as Jane Ashford, Kagura, Minerva Armitage and female Paradigm soldiers
Batsuey as Bombshell
Bobby Davison as Epsilon and Drabeus
Cedric Lutes as Zaxtor and Skyhv
Cryptdidical as Jerry
Dan Gaskil as Axon
Doom64hunter as Pheraclus
DN4Ever as Prokhor Vilmos
Eddie Teach as Geoffrey, Oveus, Jekyll, Yo, and Russian soldiers
Fernito as Alejandro
Foxley as AMC soldier and Peter Davids
Jaap van der Wulp as MJ12 Soldier
James Stanfield as himself, Naaldir, and Jedrik Demonhorn
Leo Sierras as Cultist
Maarten Van Oostrum as himself
Major Tom as Rusty Nails, Le Sang and Gabe (he dedicates his Le Sang performance to the late John Hurt)
Marty Kirra as Magnus Giesler
Maria as Lilith
Mblackwell as EDF Soldier (intro cutscene) and Vladmir
Merlijn Van Oostrum as himself
Michael Crisp as himself
Mikko Sandt as himself
Redead-ITA as Maddrax
Rhaisher as MS-Corp scientist
Sangelothi as EDF Soldier (Gameplay)
Sangluss (aka. Sangman) as himself
Steb as Highwire
Thanatos as Trenton Solari
Yemiseika as Alea
Zeraphinda as Freija
Zero X Diamond as General Graves and male Paradigm soldiers

Thanks to Brandon C Hovey and .Mark for previously voicing Axon and Trenton Solari
          and Rusty Nails, Rob 'Highwire' Wijkstra and Geoffrey for previously voicing themselves.

Code
====
James Stanfield
Sangman
Doom64hunter
Mikko Sandt
Cedric "Zaxtor" Lutes
Dan "Danarama" Gaskill (aka. DeeperThought)
Jonathan Strander (aka. Mblackwell)
Fox Martins (aka. ILOVEFOXES)
Eddy Zykov / Balls of Steel Team
Maxim Chinyakin (aka. Lezing)
Phillip Kutin (aka. HelixHorned)
Conrad Coldwood
Hunter_Rus
Lord Misfit
Hendricks266
M210
George W. Bernard
http://www.rodedev.com/tutorials/gamephysics/


Art
===

Al MacDonald
Adam Foster
Alun Bestor (for village textures)
Andre' LaMothe
Angry Meteor
Amon
Amuscaria (for Demon Eclipse sprites)
Arima
Aleh Lipka (for compass artwork)
arikel (for seamless textures)
agaures
Barn
Beast (For smoke FX)
BMacZero (for seamless textures)
Blarumyrran
Blasphemer team (for textures)
Blox
Bryant Arnett
Cage
Captain J
Carl Olsen (For flame icon)
cogitollc (for sun sprite)
Crista Forest (for textures and sprites)
Cethiel (For various textures and sprites)
Captain Toenail
The Chayed - KIIRA
David G
David Revoy (young knight portrait)
Delapouite (for backpack, silver bullet, circle, cargo crate, cardboard box, mono robot, battle mech icon)
Demolition Software
DoomGuy II
Doomjedi (for russian sign fixing)
DoomNukem (for various sprites)
DunkelSchwamm
Duion (for space backgrounds)
Emeres (for Hexagon Grid - 1)
eleazzaar (for medieval sign art)
FreeDoom team
Franco Rivolli (for fantasy artwork)
Fupi (for smoke vapor art)
gargargarrick (for fantasty window art)
Gabriel Crown "Wolf"
Gabrielwoj
Geronimo Fonts (for Guardians font)
George-W. "Jaco" BERNARD
Glen Payne (Osiris TC)
Grumbel
hc
HOPLON
Henning Horstmann
Huy Pham
idGamer
J. W. Bjerk
jellyfish0 (for particle effect spawns)
JCW (for  wood texture)
Jeff Ottinger (for dirt texture)
John Colburn (for various GUI icons)
Jonathan Harris (for something strange font)
Justin Nichol (for flare portraits)
Jolteon 3D
Kahveh Robinett
Kelvin Shadewing (for font)
KdiZd Team
Kenaz (for wormhole clip)
Kracov
Lorc (for gem, knife, raygun, ghost, spectre, missile swarm, opening shell, scale armour, cross mark icon)
Luke.RUSTLTD
Lt. Blam
Larabie Fonts
littlewhitemouse (for various artwork)
Marty Kirra
Ben "Makkon" Hale (for textures)
MarlboroMike2100
Marshal Bostwick (Osiris TC)
mike12
Monolith
M�ns Greb�ck for Merry Christmas fonts
Milosh--Andrich
Mohammad Alavi
Mor'ladim
MrBeast
Mr. Bubble
NeoWorm
NMN
Nightwatch team (for textures released)
osjclatchford
Of Far Different Nature (for dark fantasy interface pack)
Pisstepank
Phelan Riessen (for halloween font)
Phoenix (for assembling Alun Bestor texture pack into art file)
POW Studios
Quanto
Qubodup
raymoohawk
Rawdanitsu (for various artwork)
Reaver
Rick Randy
Roger Ritenour
SS2 SHT-UP project team
Sock
sbed (for cancel icon)
Skoll (for glock icon)
Scripten
Sinestesia (for blood sprites)
StumpyStrust (for space texture)
Spiney (For skyboxes)
Scuba Steve
Shambler
Shawn Harkin "The Assassin"
Spiralgraphics
Steve Alber (for planetary maps)
Stilgar
SW Last Warrior team artists (Roger Tweedie, Oliver Michel, Ryan Wine)
The Funktasm
TerminusEst13 (for rusty hat graphic)
TheFly
Tomislav Spejic
torridGristle
Tom DiLazaro
texturesforplanets
toadking07 (for SCP warning signs)
tudy1311
Tyranno1 (for main menu lensflare)
Unagi
ukiro (for OTEX texture set)
usr_share (for Grafx2 fonts)
Unnamed (For planet sprites)
Ulukai (for space environment maps)
Vader
Vladimir Nikolic (for mockery font)
Yahtzee (for book covers + poster referencing his Age of Evil mod)
WildWeasel (for magazine sprites + sleeve)
zrrion the insect (for misc sprites

Paintings
====
Hugh J. Yeman for the Last Redoubt painting

Duke 3d HRP credits
====
0815Jack
ADM
Alexander Filippov
AlgorithMan
Conrad Coldwood
Another Duke Fan
Armando
Audiocraz
BlitZ
Cage
chicken
Daedolon
Devastator
Ding Bat
DLT
dood!
Dreams
DukeAtomic
Ecmaster76
endmilled
Fearpi
Flacken.WS
Gambini
Gman
gt1750
Hendricks266
Impact
JaJo
Jblade
jimbob
Kef Nukem
Kev_Hectic
Kevin9er
Killd a ton
L2theKING
Lemming
Marked
Megadeus
motionblur
NightFright
Night Hacker
nyne
ooppee
ozz
Parkar
Piccolo
Piterplus
princetonEO
Rellik66
Roger
Quakis
Semicharm
Sky Fox
Steveeeie
SwissCM
Tesserex
theRobot
toadie2k
Tip
Vasilinka
Viciarg
WarHammer
White Alven
yossa

Models
====
Major thanks to Teamonster for donating Duke3D enemy models to the project, on which the Cycloid enemy renders were based.
(Lizard Trooper, Pigcop, Recon Car, Enforcer, Battlelord, Slimer, Slimer Egg, Alien Queen)

3absiso (for bed model)
3D_artist_vikash (for futuristic SMG, sci-fi Magnum)
3DHaupt (for Railgun prototype, Dragon, sci-fi cockpit, and rifle model)
810 dude, Eugene Ivanov, Stefan Weckert for Marathon models

Alena (for Kalestra's book model)
ali-rahimi-shahmirzadi (for souless mask model)
AlessandroFortuna (for steampunk shotgun model)
Anwar Ali (For frying pan model)
Aries063 (for Pribor-3B model)
@dannaki_ (for Quake super nailgun model)
Adamossak (for Stylized fantasy knife model)
akselmot (for steampunk rifle model
AlanBah (for fireball launcher)
Atomic Banzai (for cart model)
alexeyshadrin80 (for thermal detonator model)
agrimor (for geoffrey's prox grenade model)
Adan (for grey alien, chitter and scum model)
aswin.baskaran (for big gun model)
Alfred De Smet (for sci-fi laser gun model)
Aaron Lutes (for blunderbuss model)
Autem.mortale (for RPK model)
Art_SeTter Factory (for Dumbell model)
Artem Goyko (for His rose model)
Artsem Kalitsenya (for Golem heart model)
antoinepavia (for ninja girl, Ganjaarl model)
Aidlez R (for Classic sofa model)
Aksyonov (for Axe Mini Valor model)
AbyssLeo (for various models)
bawisan (for armoured vest model)
Bandit (for UMP45 model)
beardofsocrates (for vulpine fox head model)
buh (for Winchester 1897 shotgun model)
Blender3D (for dynamite model)
benhickling1 (for snowfall's grenade model)
Blueoxel (For Barrel-11 handgun)
Blackrayne (for various prop models)
Beatriz Frankenstein (for moonlight sword model)
BunnyLoveSu (for Whisk gun model)
clintbellanger (for Magic Mace model)
Cinnamine (for military laser, lazer module model)
Corey Keeling (for sci-fi low poly pistol model)
chiwei, YippieKY, and Mathew O (for O-Tech reaper model)
carlosvasc670 (for large medikit model)
Charlie Tinley (For Crossbow model)
Carlos_ZM (for super shotgun model)
CaptainToggle (for Nemesis sniper model)
Cyril43 (for potion bottle, throwing axe, bolter, mace, energy shield, and sword model)
Chaitanya Krishnan (for flesh squirter model)
chervinin (for Hell motorgoat)
Comrade1280 (for DSS Harbinger model)
Cameron Chung Art (for RD-100 model)
curichenkow (for quad plasma gun)
DESIGN BUREAU (for Torii Gate model)
Devin Eggleston (for Skull staff model)
doodlos (for hunter pistol)
DM-913 (for cyclops, Maurader, Numen drone model)
Danya (for winchester model)
d880 (for med-kit model, flame grenade model)
Domindik (for the last evil model)
Dimitri.Rey.3D (for Sci-fi handgun model)
doomsentinel (for jump gate model)
DelthorGames (for MP7 model)
Design Bureau (for Torii gate model)
DarkTenshiDT (for Joker's knife model)
DJMaesen (for fantasty rifle model, overlord, female spec ops, strong knight, cultist, Soldat, grenade launcher, and Shorty Shotgun model)
dakotakimble (for Wasteland shotgun model)
dennish2010 (for Futuristic Combat jet model)
Efarys (for Paladin shield and mace model)
Eccentres (for dragon slayer knife model)
Enn Shtyka (for Magic Crystal model)
finn163 (for future shotgun FLN model)
Frostoise (For nanite AR model)
Finbass (for sci-fi minigun model)
fesukre (for RPKM model)
Freddy drabble (for laser rifle model)
fikriemre (for Shaman mace)
filip (for Razor model)
Francesco Coldesina (for telescope model)
Fearell (for Sci-fi assault rifle model)
filiphan (for boxing bag model)
fabiotambone (for EMP grenade model)
fletcherkinnear (for particle beam, security cyborg model)
gentlemenk (for Hover tank model)
gustavsx12 (for toy mashine gun model)
gunasekharpaidi (for grenade launcher model)
green eyesman (for quietus and combat knife model)
Gomuz (for James' grenade model)
3DHaupt (for chainsaw model)
HardIdea (for HIE Dagger n5, demonic axe model)
hoschu (for large sci-fi medikit model)
heedongq (for James' prox bomb model)
hoefslootjons (for Zaxtor's pipebomb model)
Hunter wiltse (for sci-fi grenade model)
Herr Wolf (for Sci-fi SMG model)
Inject4 (for Hades carrier model)
ihaterendering (for prehistoric fish model)
Iliya_Luchkiy (for medikit model)
iceboxX708 (for ornamented sword model)
Jay (for ice axe model)
Jan Esch (for Zaxtor rifle model)
Jack Bronswijk (for switch blade model)
Jefferson Frenay (for Ballista model)
Jamesblake3d (for Trident flier model)
Jadon_TheArtist (for compound bow model)
J_SON (for healing potion model)
JonnyMANSON (for assassin model)
Jakob Pihl (for Dragonslayer model)
Jau (for nosferatu head model)
Juangamer (for archer model)
jitspoe (for paintball2 textures)
kado3D (for anti-matter pulveriser model)
Kimbly (for Katana model)
Kayla Hill (for Arcane spellbook model)
konradbialowas (for plasma blaster model)
kotbfg (For Doom 3 Plazmatron)
KIFIR (for old sword model)
Kurilovich Aleksey (for PDW LSC7.5 model)
Khoa kobra_k88 Nguyen (For Priss Model)
Kaan Tezcan (for ancient character model)
kastaniga (for Radar dish model)
@KyraTech_13 (for overtone flier model)
larrynguyen (for federation strike flier model)
Lonewolf (for LR300 model)
Leo_Newman (for GL-06 model)
Leksandr (for support drone model)
LeviWalker1999 (for Japanese shrine bridge model)
Lorenzo Dragotto (for capsule gun)
Luciano Soares (For bow and arrow model)
Lio Pearson (for keycard model)
LowSeb (for medieval sword model)
Loic (for fire extinguisher model)
LTadas (for modern shelf model)
morepolys (for M41A model)
hwoarangmy (for Monk model)
Maxaal (for potion model)
.Mark (for creating 3D model of Cycloid mothership)
Mainfable (for Micky's grenade model)
malf05 (for SR-2 model)
MisterH (for spacestation model)
Mateus Schwaab (for three ring wood barrel model)
Maxim Gehricke (for icicle and bone spike projectile model)
Maxim Lukyanov (for Stun grenade model)
Mambra Negra (for Assault rifle PBR model)
MonteGargano (For warmonger sword model)
Martin Jencka (for blade of basilus model)
Maria del Mar Martinez (for ultimate assault shotgun model)
Matija �vaco (for wild west and sci-fi sniper model)
Maty (for medikit model)
Meee (for Axe model)
mupp (for steampunky shotgun model)
Matthew (for Jungle crystal dagger model)
Monhoo (for treasure chest model)
MooKorea (for Fishie mech model)
Matthew (jeandiz) (for rotary cannon model)
MilaHigher (For book of the moon model)
mqpmqp (for SMG model)
MOJackal (for large diving tank model, radar dish model)
Millenia (for various models)
Murilo Freitas (for Annihilator "Type G" model)
Nexon (for various models)
nestofgames (for old bed model)
Naomi Raeien Adriaens (for UDP-9 model)
Nerd_Sp (for chainsaw hand model)
nataliedesign (for Heartbreaker crossbow)
nofaced3d (for sci-fi assault rifle model)
NeoSpica (for hydro door sound)
n3600 (for steampunk pistol model)
no12u (for Mauser pistol model)
Nathan.Luchstonstein.Goodman (for Sci-fi rocket launcher model)
Noob Model :D (For Magic Ring YingYangBlue)
Northcliffe (for Demon Knight model)
Naudaff3D (For Sterling model)
Naky (for HK CAWS model)
Nudluria (for Grindstone model)
Nobiax (for Bomb model)
NiceModels (for Hyperextension model)
omgitstallin (for Gavil model 3861N)
ONEPUT (for sci-fi gun model)
oscar creativo (for soldier armour model)
Orbelin_Jaimes (for alien suit model)
patspet (for Samurai Helmet model)
pepedrago (for M41A model)
ProgrammerNetwork (for various models)
Pieter Ferreira (for mossberg model)
Paranoia Team (for various item models for Highwire)
Ptis (for hoverbike and sci-fi assault rifle model)
Pixel Make (for Rune cannon model)
proskomid (for steampunk handgun model)
pixelgrapher (For Heavy assault rifle model)
Piacenti (for knight model)
Peanut1105 (for Evil staff model)
qiwi (for high poly dagger model)
racer445 (for future-o-tech sniper)
Ray (for deep sea fish model)
Rectus (for black powder bomb model)
reddification (for Magic Orb)
robnewman76 (for sci-fi missile model)
Riyad (for Gun2 model)
Roborail1 (for panzer-3 model)
reddification (for magic orb model)
Ridgeman (for slugshot model)
Ramhat (for Sci-fi laser rifle, futuristic modular rifle)
REMande (For Mac-10 model)
Romeo (for Grimm 12G Devastator model)
rubenve (for Chevalier sword model)
sarychev.r (for sci-fi knife model)
sader (for sader's gun)
Sam Carrier (for swirl bottle potion bottle)
Stalpaert_Arne (for Guandao model)
sanyaork (for Demon model)
samolka (for Treadmill model)
SKYE (For humanoid Tiger model for Snowfall body)
Sparkwire (for Necronomigun)
Seyquil (For Thorus model)
SGold8556 (for blade gun model)
SGTIncogniTO (for black rose revolver)
Sergey Egelsky (for shield model)
Sergey Yaushev (for Dirty shotgun model)
Serhii Denysenko (for RGD5 grenade model)
SamTheCaribbean (for Xen rifle model)
sergusster (for Inquisitor 5000 model)
Spellkaze (for medium medikit model)
Soidev (for quad barrel shotgun model)
space_potato (for russian survival knife model)
soidev (For Flowers model)
sterbi13 (For magic potion model)
SoulSlayer (for M4-15 base gun model)
shepard_238 (for Hammond DFR, Pan Flute, and HSR-2 Rifle weapon models)
SmeTheWiz (for Rocket launcher model)
SuNsH1Ne (for Gauss Rifle model)
skartemka (for RAINIER AK model)
Stark (For Crytsal Lotus model)
Star Wars Quake team (For Hovercraft model)
Trash (for Dark Elf Ballista model)
thomasvansteelandt (for WW1 grenade model)
thanoidiscominggaming (for Alof with Single Shotgun model)
TooManyDemons (for K9 Ravager model, BFG Chainsmoker model)
Torpedosheep (for elemental launcher model)
Thunder (For cannon model)
tgalextg (for low poly magnum model)
Thomas.Osterhammel (for Crossbow model)
Teliri (for low poly dagger model, drakefire pistol)
Tovarish'Mraz' (for gold shield model)
TehSnake (for Type 19 model)
Th�o Richard (for magic lantern model)
tediuminteractive (for small medikit model)
Terry Tan (for unnamed gun model used for Unmaker)
tanbe2010 (for Plasma Rifle model)
TiZiana (for Goth female - Fleur du Mal model)
Urizel (for bowie knife model)
unleasharun (for shuriken model)
Unknown000.DLL (for crying head enemy)
Valdos (for COIL model)
Varatin (for Sci-fi SMG and Assault drone model)
Valery Kharitonov (for PP energy bizon model)
Veerle Zandstra (for utility knife model)7
Vedoxox (for airship model)
Warkarma (for TNT sack model)
Wolfgar74 (for alchemy table model)
willpowaproject (for Hell demon Keris dagger model)
WillyG99 (for MP5SD model)
X-ray (for ice sword model)
Ya cov (for LCD model)
Yksnawel (for stg.58 and Mossberg590A1 model)
Zakyora the clown (for motorcycle helmet model)
??? (for spaceship model)


Sounds
=====
32cheeseman32 (for leather grip sound)
16fpanskasimonmartin (for ice chopping sound)
Adam Webb (for creepy laugh)
astrar (for saw sound)
Aetherspire (for roaring sounds)
alegemaate (for keycard unlocking sound)
amliebsch (for voices from hell sound)
Ama_dis (for washing machine sound)
akelley6 (for error sound)
aleks41 (for magic casting sound)
alkanetexe (for anime sword attack sound)
Ali_6868 (for arrow impact sound)
arseniiv (for rusty sound)
alonsotm (for spell sound)
astrar (for saw sound)
AnLorenzo (for jacuzzi sound based on jasonmchl, BoilingSand and visions68 sounds)
arnaud-coutancier
arun reginald (for eerie metallic noise)
bigmanjoe (for beep map sound)
Benboncan (For ship bell sound)
Bryant Arnett
Bryan Wysopal (for tavern music)
Bart K
bird-man (For servo sound)
biancabothapure (for frog sound)
Batuhan (for Fire sound)
bird-man (for chargeup sound)
bajko (for forest ambience sound)
bychop (energy shield recharging sound)
Chris Murray (for hand dryer sound)
calebmelcher (for monster sound)
cs279 (for interface sound)
Circlerun
caitlin-100 (for wine bottle screwing unscrewing sound)
Cafafo (for bar ambience sound)
CosmicEmbers (for chain snapping sound)
cbakos (for click sound)
cusconauta (for metal hitting sound)
CosmicD
CGEffex (for grenade pin sound)
charliewd100 (for sci-fi gun sound)
cheeseheadburger (for gun battle sound)
cell31
cmusounddesign
cyberkineticfilms
d4xx (for horses galloping)
Digifishmusic (for various sounds)
Deganoth (for various monster sounds)
djgriffin (for tibetan chanting sound)
duckduckpony (for gun drawing sound)
Dzierzan, Shadow (Ninjakitty), Akis_02 (for shadow warrior, Exhumed hi-res sounds)
Eddy Zykov
Emma_MA
esperr (for chainsaw sound)
erdie (for bow sound)
Erokia (for beacon sound)
Ekuhvielle (for beeping sound)
EFlex the sound designer (for building collapse sound)
erh (for eerie strings)
erdie (for sheep sound)
ejfortin (for energy sounds)
epicdude959 (For creepy ghost scream)
Fantozzi
FreeDoom team
Freesound.org
fins (for error sound)
ftpalad (for flurescent light sound)
fl1ppy (for medical bay sound)
funkymuskrat (for katana chil sound)
FXHome
Fins (for teleport sound)
kyles (for neon, turbine, and flag flapping sound)
GoBusto
Gabemiller74 (for ghost sound)
geodylabs (for rain hitting window sound)
GoodSoundForYou (for typhoon sounds)
grokowsky (for vending machine hum)
gordeszkakerek (for keycard sound)
glitchedtones (for wasp drone sound)
HarleyGlitch (for glitch sound)
Hellska (for barrel hit sound)
H2x (for sci-fi door sound)
Hakren (for piano ambient music)
Huminaatio
humanoide7000 (for metal scrape sound)
husky70 (for can crushing sound)
hybrid (for various sounds)
henrique85n (for van stopping sound)
iberian-runa
indigoray (for translator beep)
icyjim (for cloverfield roaring sound)
ivolipa (for rain hitting ground sound)
ianstargem (for whisper sound)
inspectorj (for synthesized wind, organ, wind chimes, bamboo swing sound, stretching sound)
jdagenet
javierzumer (for charging sound)
josepharaoh99 (for snowfall pickup sound)
JustinB (for ice cracking sound)
Julien Matthey (for fireball sound)
jobro (for beeping and explosion sound)
Jojikiba
jorickhoofd (for frustrated man screaming)
JoelAudio (for smashing sound)
johnsonbrandediting (for male panicking sounds)
JPolito (for locked door sound)
James Tubbritt (for Sci-fi sounds)
juancamiloorjuela (for health pickup sound)
knova
kgatto
kinoton (for firework launching)
keweldog (for dog panting sound)
kickhat (for war horn sound)
kennysvoice (for robot voice sounds)
kingasmas (for Depressurization sound)
klankbeeld (for various horror sounds)
larko35 (for organic click sound)
Lee Barkovich
Leady (for locked door sound)
LittleRobotSounds
Litruv (for ghost whisper sound)
ljudbank (For spooky ambient sounds)
leonelmai (for Gieger counter sound)
LloydEvans09 (for Deep Gong ambience sound)
legolunatic (for laser charging sound)
MaxThrower (for digging sound)
Mirko Horstmann
Mass Effect: Discovery project (for PATCOS sounds)
mickyman5000 (For metal smashing sound)
mrthomas2000 (for frying pan sound)
MT Johnson (for Toast popping sound)
M-RED (for phasing beam sound)
minigunfiend (for creature scuttle sound)
Mozfoo (for growling sound)
MrAuralization (for cassete sound)
Natemarler (for nailgun sound)
Nicistarful (for lever sound)
newagesoup (for Warhorn sound)
newlocknew (for waterfall sound)
nekoninja (for various sounds, samurai slash .etc)
Navaro (for lever action reload sounds)
nioczkus (for garand firing sound)
NoiseCollector (For Spacesuit sound)
noiseinten (for diesel train sound)
noirenex (for sci-fi alarm and deep scan sound)
ollieollie
original-sound (for wooden alert sound)
onderwish (for emergency siren sound)
OSFX (for space hanger sound)
osiruswaltz (for various ghostly sounds)
pepingrillin (for armour pickup sound)
pfranzen (for mashing keyboard sound)
pcruzn (for ghostly transition sound)
passAirmangrace (for snoring sound)
PaulMorek (for squish sound)
pgi (for minigun loop)
porkmuncher (for swinging sound)
ppfpower87 (for locked door sound)
PaulDihor (for transform sound)
Prinsu-Kun
Pogotron (for train sound)
primeval-polypod (for pneumatic door sound)
peridactyloptrix (for Hovertank moving sounds)
qubodup (for military vehicle, various sounds)
reinsamba
reidedo (for hawk sounds)
Resaural (for tesla sound)
ryansnook
ryanconway
rylandbrooks (for motorcycle helmet sound)
runningmind (for special ammo, shield sound)
Robinhood76 (for car door, sci-fi door, goblin spell, monster sounds, stinger)
RutgerMuller (For curtain sound)
suburbanwizard (for chanting sound)
Sea Fury (for various monster sounds)
SoundFXstudio (for missile launch sound)
soulbaby71 (For pinball machine sound)
snapper4298 (for camera sound)
sabovot (for wolf running sound)
sonically_sound (for countdown sound)
Sclolex (for cave dripping sound, apparation sound)
soneproject (for ecofuture various sounds)
shahruhaudio (for robot sound)
Stefan021 (for projector sound)
SFX by Circlerun
skyklan47
shelbyshark (for garand ping sound)
sypherzent (for armour hit sound)
spookymodem (for goblin sounds)
SoundJay
superphat (for various weapon sounds)
suanho (for metaphisical death stinger)
Sojan (for various sci-fi sounds)
soundscalpal (for firework sound)
Sounddogs
sfstudios
Soundranger
spanrucker (for toaster clicking sounds)
SoundEffectsFactory
soykevin
Stephen Meli
spennny (for sci-fi sound)
supakid13 (for zoom sound)
timbre (for various sounds)
tec_studio (for hawk sound)
TheSoundcatcher (for metal smash sound)
themfish (for bulb smashing sound)
Tristan (for spaceship ambient sound)
tmfksoft (for chair piston sound)
tome02 (for organ sound)
ThefitzyG (for gore sound)
tmkappelt (for zippo sound)
twisterman (for weapon pickup sound)
TinyWorlds (for Retro explosion sound)
the_yura (for Air clap sound)
TristanLuigi (for big explosion sound)
The Recordist
under7dude
unfa (for energy and sci-fi menu sounds)
vabadus (for M203 gl sound)
visions68 (for steam sound)
Vicces1212 (for treasure sound)
Vartioh (for tesla monster sound)
wjl (for fireball sound)
xtrgamr (for TV turning off sound)
Yap_Audio_Production (for locked door sound)
YOH (for train sound)
yks (for ant scurry sound)
zachrau (for pig sounds)
zenithinfinitivestudios (for fantasy UI sound)
zajjman (For blossom barking sound)
Zimbot (for squishing sound)
zivs (for ammo pickup sounds)

Music (all OGGs are tagged with author information where possible)
=====
Alexandr Zhelanov (https://soundcloud.com/alexandr-zhelanov)
artisticdude
Awesome
Aspecty
Alex McCulloch
Brandon Morris
Bogart VGM (https://www.facebook.com/BogartVGM/)
cinameng
Capcom
Captain Creepy (https://Captaincreepy.bandcamp.com/album/still)
cynicmusic
shalpin
Chris Robinson
Dark Luke
DJChrismix01
Eric Matyas (sector off limits)
Kim Lightyear (https://soundcloud.com/kl-gametunes)
Fossergrim ambient
Fantasy_Origins
Felephante
Gobusto
generalska (for winter wonderland)
Gundatsch (https://soundcloud.com/gundatsch)
grindhold
Headfirst
HitCtrl (https://soundcloud.com/hitctrl)
HorrorPen (http://opengameart.org/users/horrorpen)
Huy Pham
Insydnis
Jason Dagenet
Joshua Stephen Kartes
likantropika (Random gods)
leohpaz
Mike Norvak
Muciojad
makai-symphony (https://soundcloud.com/makai-symphony/tracks)
Michael Klier (haunted woods loop)
neocrey
Nine Inch Nails
Nikke (for dark intro)
Peter John Ross (sonnyboo.com)
Patrick de Arteaga
Pheonton
osiruswaltz
Ove Melaa
oglsdl
Oddroom
rBrn
SouljahdeShiva
syncopika
Scrabbit
Spring Spring
Socapex
Spring
tebruno99
Ted Kerr
Tsorthan Grove (Kingdom of Icereach)
Templar (Fallen Champion)
Trevor Lentz (Salvation, Deus Ex Tempus)
The Cybershock for Tetrinet remix
ViRiX Dreamcore (David McKee soundcloud.com/virix)
Vartioh (for synth pulse pad)
wipics (for Winter Wind)
yd
Zander Noriega (https://soundcloud.com/zander-noriega)


Special Thanks
======
Richard Gobeille (aka. TerminX)
Jonathon Fowler (aka. JonoF)
EDuke32 team & contributors
Voidpoint
TNT Team
3DRealms
Valve

-----------------------------------------------------
In memory of our friend Cedric 'Zaxtor Znort' Lutes.
-----------------------------------------------------
