<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
  <id>io.itch.amcsquad.amcsquad</id>
  <launchable type="desktop-id">io.itch.amcsquad.amcsquad.desktop</launchable>
  <name>The AMC Squad</name>
  <branding>
    <color type="primary" scheme_preference="light">#dadada</color>
    <color type="primary" scheme_preference="dark">#1f244d</color>
  </branding>
  <developer id="io.itch.amcsquad"><name>AMC Team</name></developer>
  <summary>Retro FPS with modern sensibilities</summary>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-2.0</project_license>
  <url type="homepage">https://amcsquad.itch.io/game</url>
  <url type="help">https://amctc.fandom.com/wiki/AMC_TC_Wiki</url>
  <url type="contact">https://discord.gg/aCqnMAZ</url>
  <url type="vcs-browser">https://github.com/dibollinger/amcduke32</url>
  <description>
      <p>The AMC Squad is an epic story-driven FPS running on the Build Engine. It combines gameplay mechanics of retro-shooters with aspects of modern titles, complete with a story-driven narrative that spans over several episodes.</p>
      <p>Each episode is a full-length campaign featuring multiple varied locations to explore and many hidden secrets and collectibles to discover. Experience an overarching story that is loosely based on the worlds and settings of other classic Build Engine games. Pick from more than 8 different characters, each with a unique set of items, weapons and abilities, and fight a large selection of different enemy factions that threaten the safety of the world.</p>
      <p>The game currently contains 4 fully playable episodes, with 6 episodes planned total. Episode 5 is currently in development. No release date estimate is available at this time.</p>
      <p>Features:</p>
      <ul>
        <li>Classic FPS gameplay mixed with modern mechanics.</li>
        <li>Mission-based level structure, spanning 4+ separate episodes, with more than 20 hours worth of content.</li>
        <li>Intricate and highly interactive levels, with many secrets to find.</li>
        <li>A rich story consisting of multiple character arcs and plot threads.</li>
        <li>8 different characters with different gameplay styles, with more to unlock as you progress.</li>
        <li>Unique loadout of weapons and items for each character, as well as level-specific weapons to find and unlock.</li>
        <li>Collectibles, weapon replacements and a fully-fledged progress system.</li>
        <li>Upgradeable weapons and character abilities.</li>
        <li>Multiple vehicles, including motorcycles, spaceships, tanks and horses.</li>
        <li>In-game cutscenes and pre-rendered FMV sequences, complete with voice acting and subtitles.</li>
        <li>Original music composed by HiveBoss, TheDavisD, AliCatGamer, and various tracks sourced from OpenGameArt.</li>
        <li>And more features you never thought possible in a Build Engine game!</li>
      </ul>
  </description>
  <screenshots>
    <screenshot type="default">
      <image type="source">https://img.itch.zone/aW1hZ2UvMTQ3NTQzMy84NjA1NjIwLmpwZw==/original/kODu0x.jpg</image>
      <caption>Episode 4: Church</caption>
    </screenshot>
    <screenshot type="default">
      <image type="source">https://img.itch.zone/aW1hZ2UvMTQ3NTQzMy84NjA1NjE2LnBuZw==/original/BlhN%2Bn.png</image>
      <caption>Paradigm update</caption>
    </screenshot>
    <screenshot>
      <image type="source">https://img.itch.zone/aW1hZ2UvMTQ3NTQzMy85NjUzMzE3LnBuZw==/original/s4rVpI.png</image>
      <caption>Episode 4: A mystic library</caption>
    </screenshot>
    <screenshot>
      <image type="source">https://img.itch.zone/aW1hZ2UvMTQ3NTQzMy85NjUzMzE2LnBuZw==/original/XRNZII.png</image>
      <caption>Episode 4: Spaceship hangar</caption>
    </screenshot>
    <screenshot>
      <image type="source">https://img.itch.zone/aW1hZ2UvMTQ3NTQzMy84NjA1NjE1LnBuZw==/original/TJ4xtx.png</image>
      <caption>Elysion</caption>
    </screenshot>
    <screenshot>
      <image type="source">https://img.itch.zone/aW1hZ2UvMTQ3NTQzMy85NjUzMzE5LnBuZw==/original/3WTy%2Bc.png</image>
      <caption>Episode 4: Unlockable playable characters</caption>
    </screenshot>
    <screenshot>
      <image type="source">https://img.itch.zone/aW1hZ2UvMTQ3NTQzMy85NjUzMzE4LnBuZw==/original/GkYPcf.png</image>
      <caption>Episode 4: Friendly NPCs</caption>
    </screenshot>
    <screenshot>
      <image type="source">https://media.moddb.com/images/games/1/17/16430/Sol_system.png</image>
      <caption>The mission map on the solar system</caption>
    </screenshot>
    <screenshot>
      <image type="source">https://media.moddb.com/images/games/1/17/16430/mickyselect.png</image>
      <caption>Character selection screen</caption>
    </screenshot>
    <screenshot>
      <image type="source">https://media.moddb.com/images/games/1/17/16430/duke0001.png</image>
      <caption>The R&amp;D lab, where you can research equipment and other upgrades</caption>
    </screenshot>
    <screenshot>
      <image type="source">https://media.moddb.com/images/games/1/17/16430/amc0001.1.png</image>
      <caption>The mission control map, showing missions to play and resource facilities to buy</caption>
    </screenshot>
    <screenshot>
      <image type="source">https://media.moddb.com/images/games/1/17/16430/NEWSPR3.jpg</image>
      <caption>A pre-mission briefing cutscene, assembling the entire squad in the mission control room</caption>
    </screenshot>
  </screenshots>
  <categories>
    <category>Game</category>
    <category>Shooter</category>
  </categories>
  <releases type="external" />
  <content_rating type="oars-1.1">
    <content_attribute id="violence-cartoon">intense</content_attribute>
    <content_attribute id="violence-fantasy">intense</content_attribute>
    <content_attribute id="violence-bloodshed">intense</content_attribute>
    <content_attribute id="drugs-alcohol">moderate</content_attribute>
    <content_attribute id="drugs-tobacco">moderate</content_attribute>
    <content_attribute id="sex-nudity">moderate</content_attribute>
    <content_attribute id="language-profanity">moderate</content_attribute>
    <content_attribute id="language-humor">intense</content_attribute>
  </content_rating>
  <update_contact>https://amcsquad.itch.io/</update_contact>
  <supports>
    <control>gamepad</control>
  </supports>
  <recommends>
    <control>pointing</control>
    <control>keyboard</control>
  </recommends>
  <provides>
    <binary>amcsquad</binary>
  </provides>
</component>
